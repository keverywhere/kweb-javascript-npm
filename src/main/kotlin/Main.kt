import kotlinext.js.require

fun main() {
    require("awesome-notifications/dist/style.css")
    with(Notifier()) {
        tip("tip")
        info("info")
        success("success")
        warning("warning")
        alert("alert")
    }
}