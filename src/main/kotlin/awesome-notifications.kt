@file:JsModule("awesome-notifications")
@file:JsNonModule

@JsName("default")
external class Notifier(options: Any = definedExternally) {
    fun tip(msg: String, options: Any? = definedExternally)
    fun info(msg: String, options: Any? = definedExternally)
    fun success(msg: String, options: Any? = definedExternally)
    fun warning(msg: String, options: Any? = definedExternally)
    fun alert(msg: String, options: Any? = definedExternally)
}
