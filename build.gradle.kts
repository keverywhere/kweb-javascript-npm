plugins {
    id("org.jetbrains.kotlin.js") version "1.4.10"
}

group = "org.gitlab.denissov.kweb-javascript-npm"
version = "1.0-SNAPSHOT"

repositories {
    maven("https://kotlin.bintray.com/kotlin-js-wrappers/")
    mavenCentral()
    jcenter()
}

kotlin.target.browser { }

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation("org.jetbrains:kotlin-extensions:1.0.1-pre.123-kotlin-1.4.10")
    implementation(npm("awesome-notifications", "3.1.1"))
    implementation(npm("css-loader", "4.3.0"))
    implementation(npm("style-loader", "2.0.0"))
}
